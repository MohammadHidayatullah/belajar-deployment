import React from 'react'
import Car from '../../assets/img/car.svg'
import '../../assets/css/Home.css'
import { Link } from 'react-router-dom'

function Hero() {
  return (
    <>
        <section id="hero">
        <div class="container">
            <div class="row py-5 align-items-center hero">
                <div class="intro col-lg-6 col- 12">
                    <h1>Sewa & Rental Mobil Terbaik di Kawasan Jember</h1>
                    <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                    <Link to='/sewa'>
                    <button type="button" class="btn btn-success sm">Mulai Sewa Mobil</button>
                    </Link>
                </div>
                <div class="col-lg-6 col-12 d-flex align-self-end ">
                    <div class="bg"></div>
                    <img src={Car} className="img-car img-fluid " alt="Banner Rental Mobil" data-aos="fade-left" data-aos-duration="1000"/>
                </div>
            </div>
        </div>
    </section>
    </>
  )
}

export default Hero