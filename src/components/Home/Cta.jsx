import React from 'react'

function Cta() {
  return (
    <>
        <section id="cta">
        <div class="container">
            <div class="banner text-center p-5" data-aos="zoom-in" data-aos-duration="1000">
                <div class="col-lg-12 col-md-12 col-12">
                    <h2 class="title">Sewa Mobil di Jember Sekarang</h2>
                    <p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod <br/> tempor incididunt ut labore et dolore magna aliqua. </p>
                    <button class="btn btn-success sm">Mulai Sewa Mobil</button>
                </div>
            </div>
        </div>
    </section>
    </>
  )
}

export default Cta