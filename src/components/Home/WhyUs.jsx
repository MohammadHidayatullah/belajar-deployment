import React from 'react'
import IconComplete from '../../assets/icon/home/icon_complete.svg'
import IconPrice from '../../assets/icon/home/icon_price.svg'
import IconClock from '../../assets/icon/home/icon_24hrs.svg'
import IconProfesional from '../../assets/icon/home/icon_professional.svg'

function WhyUs() {
  return (
    <>
        <section id="whyus">
        <div class="container">
            <h2>Why Us?</h2>
            <p class="info">Mengapa harus pilih Binar Car Rental?</p>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12 d-flex justify-content-center" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="card">
                        <div class="card-body">
                            <img src={IconComplete} alt=""/>
                            <h5 class="card-title">Mobil Lengkap</h5>
                            <p class="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 d-flex justify-content-center" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="card">
                        <div class="card-body">
                            <img src={IconPrice} alt=""/>
                            <h5 class="card-title">Harga Murah</h5>
                            <p class="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 d-flex justify-content-center" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="card">
                        <div class="card-body">
                            <img src={IconClock} alt=""/>
                            <h5 class="card-title">Layanan 24 Jam</h5>
                            <p class="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12 d-flex justify-content-center" data-aos="zoom-in" data-aos-duration="2000">
                    <div class="card">
                        <div class="card-body">
                            <img src={IconProfesional} alt=""/>
                            <h5 class="card-title">Sopir Profesional</h5>
                            <p class="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </>
  )
}

export default WhyUs