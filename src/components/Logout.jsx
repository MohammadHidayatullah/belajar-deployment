/** @format */

import React from "react";
import { GoogleLogout } from "react-google-login";

function logout() {
  const clientId =
    "1092107112629-at7v71f30auqc6onspvj2ia11r5icj9c.apps.googleusercontent.com";
  const onSuccess = () => {
    console.log("Logout successfull!");
  };
  return (
    <div id='signOutButton'>
      <GoogleLogout
        clientId={clientId}
        buttonText='Logout'
        onLogoutSuccess={onSuccess}
      />
    </div>
  );
}

export default logout;
