/** @format */

import React from "react";

function Modal() {
  return (
    <div>
      <div
        class='modal'
        id='exampleModal'
        tabindex='-1'
        role='dialog'
        aria-labelledby='exampleModalLabel'
        aria-hidden='true'>
        <div class='modal-dialog modal-dialog-centered' role='document'>
          <div class='modal-content text-center'>
            <div class='modal-body'>
              <img src='images/img-BeepBeep.svg' alt='' />
            </div>
            <h5>Menghapus Data Mobil</h5>
            <p>
              Setelah dihapus, data mobil tidak dapat <br /> dikembalikan. Yakin
              ingin menghapus?
            </p>
            <div class='button-confirm mb-3'>
              <button type='button' class='btn-ya'>
                Ya
              </button>
              <button type='button' class='btn-tidak' data-dismiss='modal'>
                Tidak
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
