/** @format */

import React from "react";
import Hero from "../components/Home/Hero";
import Navbar from "../components/Navbar";
import BarSeacrh from "../components/SewaMobil/BarSeacrh";
import Cars from "../components/SewaMobil/Cars";
import '../assets/css/Sewa.css'

function SewaMobil() {
  return (
    <>
      <Navbar />
      <Hero />
      <BarSeacrh />
      <Cars />
    </>
  );
}

export default SewaMobil;
