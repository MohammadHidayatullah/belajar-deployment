/** @format */

import React from "react";
import Car from "../assets/img/car.svg";
import "../assets/css/Home.css";
import Navbar from "../components/Navbar";
import Service from "../components/Home/Service";
import WhyUs from "../components/Home/WhyUs";
import Testimony from "../components/Home/Testimony";
import Cta from "../components/Home/Cta";
import Faq from "../components/Home/Faq";
import Footer from "../components/Home/Footer";
import Hero from "../components/Home/Hero";

function Home() {
  return (
    <>
      <Navbar />
      <Hero/>
      <Service />
      <WhyUs />
      <Testimony />
      <Cta />
      <Faq />
      <Footer />
    </>
  );
}

export default Home;
