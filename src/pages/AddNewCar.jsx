/** @format */

import React, { useState } from "react";
import Upload from "../assets/icon/fi_upload.svg";
import "../assets/css/EditCars.css";
import NavbarDashboard from "../components/NavbarDashboard";
import { useNavigate } from "react-router";
import axios from "axios";
import UploadImgae from "../components/UploadImgae";
import Swal from "sweetalert2";

function AddNewCar() {
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState("");
  const [category, setCategory] = useState("");
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = new FormData();
    data.append("name", name);
    data.append("price", price);
    data.append("image", image);
    data.append("status", false);
    data.append("category", category);
    console.log(data);
    try {
      const res = await axios.post(
        "https://rent-car-appx.herokuapp.com/admin/car",
        data,
        {
          headers: {
            "content--type": "multipart/formdata",
          },
        }
      );
    } catch (error) {
      console.log(error);
    }

    navigate(
      "/cars",
      Swal.fire({
        position: "top-end",
        icon: "success",
        title: "Your work has been saved",
        showConfirmButton: false,
        timer: 1500,
      })
    );
  };
  return (
    <>
      <NavbarDashboard />
      <section class='content-section ps-5 pe-4'>
        <div class='row'>
          <div class='col-lg-0' id='show-col-lg-0'>
            <div
              class='adding-space'
              // style='width: 300px;'
            ></div>
          </div>
          <div class='col-lg-12' id='show-col-lg-12'>
            <section id='page-5'>
              <div class='container pt-4'>
                <div class='row'>
                  <div class='col-12'>
                    <div class='sub-tree d-flex mt-3 mb-3'>
                      <a href='#'>
                        <p class='fw-bold'>Cars</p>
                      </a>
                      <i class='fa fa-solid fa-chevron-right mx-2'></i>
                      <a href='#'>
                        <p class='fw-bold'>List Car</p>
                      </a>
                      <i class='fa fa-solid fa-chevron-right mx-2'></i>
                      <a href='#'>
                        <p>Add New Car</p>
                      </a>
                    </div>
                    <h2>Add New Car</h2>
                    <div class='form-body mb-3'>
                      <form onSubmit={(e) => handleSubmit(e)}>
                        <div class='row'>
                          <div class='col-lg-3'>
                            <label for='inputName' class='col-form-label'>
                              Nama
                            </label>
                            <label
                              for='wajib'
                              // style='color: red'
                            >
                              *
                            </label>
                          </div>
                          <div class='col-lg-9'>
                            <input
                              type='text'
                              id='inputNama6'
                              class='form-control'
                              placeholder='Nama'
                              onChange={(e) => setName(e.target.value)}
                            />
                            <small
                              id='namaHelp'
                              class='form-text text-muted'></small>
                          </div>
                        </div>

                        <div class='row'>
                          <div class='col-lg-3'>
                            <label for='inputPassword6' class='col-form-label'>
                              Harga
                            </label>
                            <label
                              for='wajib'
                              // style='color: red'
                            >
                              *
                            </label>
                          </div>
                          <div class='col-lg-9'>
                            <input
                              type='text'
                              id='inputHarga6'
                              class='form-control'
                              placeholder='Harga'
                              onChange={(e) => setPrice(e.target.value)}
                            />
                            <small
                              id='hargaHelp'
                              class='form-text text-muted'></small>
                          </div>
                        </div>

                        <UploadImgae
                          onChange={(e) => setImage(e.target.files[0])}
                        />
                        <div class='row'>
                          <div class='col-lg-3'>
                            <label for='inputCategory' class='col-form-label'>
                              Kategori
                            </label>
                            <label
                              for='wajib'
                              // style='color: red'
                            >
                              *
                            </label>
                          </div>
                          <div class='col-lg-9'>
                            <input
                              type='text'
                              id='inputKategori'
                              class='form-control'
                              placeholder='Kategori'
                              onChange={(e) => setCategory(e.target.value)}
                            />
                            <small
                              id='categoryHelp'
                              class='form-text text-muted'></small>
                          </div>
                        </div>
                        <div class='row'>
                          <div class='col-lg-3'>
                            <label for='inputStartRent6' class='col-form-label'>
                              Start Rent
                            </label>
                          </div>
                          <div class='col-lg-9'>
                            <span> - </span>
                            <span class='error-notif'></span>
                          </div>
                        </div>

                        <div class='row'>
                          <div class='col-lg-3'>
                            <label
                              for='inputFinishRent6'
                              class='col-form-label'>
                              Finish Rent
                            </label>
                          </div>
                          <div class='col-lg-9'>
                            <span> - </span>
                            <span class='error-notif'></span>
                          </div>
                        </div>

                        <div class='row'>
                          <div class='col-lg-3'>
                            <label
                              for='inputCreatedRent6'
                              class='col-form-label'>
                              Created Rent
                            </label>
                          </div>
                          <div class='col-lg-9'>
                            <span> - </span>
                            <span class='error-notif'></span>
                          </div>
                        </div>

                        <div class='row'>
                          <div class='col-lg-3'>
                            <label
                              for='inputUpdatedRent6'
                              class='col-form-label'>
                              Updated Rent
                            </label>
                          </div>
                          <div class='col-lg-9'>
                            <span> - </span>
                            <span class='error-notif'></span>
                          </div>
                        </div>
                        <button type='button' class='btn-custom'>
                          Cancel
                        </button>
                        <button type='submit' class='btn-custom-save'>
                          Save
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </>
  );
}

export default AddNewCar;
